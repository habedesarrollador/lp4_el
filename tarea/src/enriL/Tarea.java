    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enriL;

import enriLF.Ave;
import enriLF.Humanos;
import enriLF.Perro;

/**
 *
 * @author HABE
 */
public class Tarea extends Persona {

    public Tarea(String b) {
        this.b = b;
    }

    String b;

    public static void main(String[] args) {

        //Tarea N°1-Clase de instanciacion y Herencias
        //====================================
        Persona a = new Persona();
        a.nombre = "Enrique";
        a.edad = 23;
        // a.hablar();
        // System.out.println("Mi nombre es:"+a.nombre+"y mi edad es:"+a.edad);
        System.out.println("Mi nombre es:" + a.nombre);
        System.out.println("Mi mi edad es:" + a.edad);
        //====================================

        //Tarea N°2-Clases Abstractas
        //====================================
        Perro p = new Perro();
        p.comer();
        p.moverse();

        Ave av = new Ave();
        av.comer();
        av.moverse();

        Humanos hu = new Humanos();
        hu.comer();
        hu.moverse();
        hu.DetalleHumano();
    }

}
